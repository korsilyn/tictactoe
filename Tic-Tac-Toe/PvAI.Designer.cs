﻿namespace Tic_Tac_Toe
{
    partial class PvAI
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cell2 = new System.Windows.Forms.Button();
            this.cell3 = new System.Windows.Forms.Button();
            this.cell4 = new System.Windows.Forms.Button();
            this.cell5 = new System.Windows.Forms.Button();
            this.cell6 = new System.Windows.Forms.Button();
            this.cell7 = new System.Windows.Forms.Button();
            this.cell8 = new System.Windows.Forms.Button();
            this.cell9 = new System.Windows.Forms.Button();
            this.lblMove = new System.Windows.Forms.Label();
            this.cell1 = new System.Windows.Forms.Button();
            this.tmrWinChk = new System.Windows.Forms.Timer(this.components);
            this.tmrAI = new System.Windows.Forms.Timer(this.components);
            this.MM = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cell2
            // 
            this.cell2.BackColor = System.Drawing.Color.White;
            this.cell2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cell2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cell2.Location = new System.Drawing.Point(118, 12);
            this.cell2.Name = "cell2";
            this.cell2.Size = new System.Drawing.Size(100, 100);
            this.cell2.TabIndex = 1;
            this.cell2.UseVisualStyleBackColor = false;
            this.cell2.Click += new System.EventHandler(this.cell2_Click);
            // 
            // cell3
            // 
            this.cell3.BackColor = System.Drawing.Color.White;
            this.cell3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cell3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cell3.Location = new System.Drawing.Point(224, 12);
            this.cell3.Name = "cell3";
            this.cell3.Size = new System.Drawing.Size(100, 100);
            this.cell3.TabIndex = 2;
            this.cell3.UseVisualStyleBackColor = false;
            this.cell3.Click += new System.EventHandler(this.cell3_Click);
            // 
            // cell4
            // 
            this.cell4.BackColor = System.Drawing.Color.White;
            this.cell4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cell4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cell4.Location = new System.Drawing.Point(12, 118);
            this.cell4.Name = "cell4";
            this.cell4.Size = new System.Drawing.Size(100, 100);
            this.cell4.TabIndex = 3;
            this.cell4.UseVisualStyleBackColor = false;
            this.cell4.Click += new System.EventHandler(this.cell4_Click);
            // 
            // cell5
            // 
            this.cell5.BackColor = System.Drawing.Color.White;
            this.cell5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cell5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cell5.Location = new System.Drawing.Point(118, 118);
            this.cell5.Name = "cell5";
            this.cell5.Size = new System.Drawing.Size(100, 100);
            this.cell5.TabIndex = 4;
            this.cell5.UseVisualStyleBackColor = false;
            this.cell5.Click += new System.EventHandler(this.cell5_Click);
            // 
            // cell6
            // 
            this.cell6.BackColor = System.Drawing.Color.White;
            this.cell6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cell6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cell6.Location = new System.Drawing.Point(224, 118);
            this.cell6.Name = "cell6";
            this.cell6.Size = new System.Drawing.Size(100, 100);
            this.cell6.TabIndex = 5;
            this.cell6.UseVisualStyleBackColor = false;
            this.cell6.Click += new System.EventHandler(this.cell6_Click);
            // 
            // cell7
            // 
            this.cell7.BackColor = System.Drawing.Color.White;
            this.cell7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cell7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cell7.Location = new System.Drawing.Point(12, 224);
            this.cell7.Name = "cell7";
            this.cell7.Size = new System.Drawing.Size(100, 100);
            this.cell7.TabIndex = 6;
            this.cell7.UseVisualStyleBackColor = false;
            this.cell7.Click += new System.EventHandler(this.cell7_Click);
            // 
            // cell8
            // 
            this.cell8.BackColor = System.Drawing.Color.White;
            this.cell8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cell8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cell8.Location = new System.Drawing.Point(118, 224);
            this.cell8.Name = "cell8";
            this.cell8.Size = new System.Drawing.Size(100, 100);
            this.cell8.TabIndex = 7;
            this.cell8.UseVisualStyleBackColor = false;
            this.cell8.Click += new System.EventHandler(this.cell8_Click);
            // 
            // cell9
            // 
            this.cell9.BackColor = System.Drawing.Color.White;
            this.cell9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cell9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cell9.Location = new System.Drawing.Point(224, 224);
            this.cell9.Name = "cell9";
            this.cell9.Size = new System.Drawing.Size(100, 100);
            this.cell9.TabIndex = 8;
            this.cell9.UseVisualStyleBackColor = false;
            this.cell9.Click += new System.EventHandler(this.cell9_Click);
            // 
            // lblMove
            // 
            this.lblMove.AutoSize = true;
            this.lblMove.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblMove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblMove.Location = new System.Drawing.Point(12, 340);
            this.lblMove.Name = "lblMove";
            this.lblMove.Size = new System.Drawing.Size(0, 13);
            this.lblMove.TabIndex = 9;
            this.lblMove.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cell1
            // 
            this.cell1.BackColor = System.Drawing.Color.White;
            this.cell1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cell1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cell1.Location = new System.Drawing.Point(12, 12);
            this.cell1.Name = "cell1";
            this.cell1.Size = new System.Drawing.Size(100, 100);
            this.cell1.TabIndex = 0;
            this.cell1.UseVisualStyleBackColor = false;
            this.cell1.Click += new System.EventHandler(this.cell1_Click);
            // 
            // tmrWinChk
            // 
            this.tmrWinChk.Enabled = true;
            this.tmrWinChk.Interval = 1;
            this.tmrWinChk.Tick += new System.EventHandler(this.tmrWinChk_Tick);
            // 
            // tmrAI
            // 
            this.tmrAI.Interval = 500;
            this.tmrAI.Tick += new System.EventHandler(this.tmrAI_Tick);
            // 
            // MM
            // 
            this.MM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.MM.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MM.FlatAppearance.BorderSize = 0;
            this.MM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MM.Location = new System.Drawing.Point(224, 335);
            this.MM.Name = "MM";
            this.MM.Size = new System.Drawing.Size(100, 23);
            this.MM.TabIndex = 21;
            this.MM.Text = "В главное меню";
            this.MM.UseVisualStyleBackColor = false;
            this.MM.Click += new System.EventHandler(this.MM_Click);
            // 
            // PvAI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 371);
            this.Controls.Add(this.MM);
            this.Controls.Add(this.lblMove);
            this.Controls.Add(this.cell9);
            this.Controls.Add(this.cell8);
            this.Controls.Add(this.cell7);
            this.Controls.Add(this.cell6);
            this.Controls.Add(this.cell5);
            this.Controls.Add(this.cell4);
            this.Controls.Add(this.cell3);
            this.Controls.Add(this.cell2);
            this.Controls.Add(this.cell1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "PvAI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tic-Tac-Toe";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PvAI_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cell1;
        private System.Windows.Forms.Button cell2;
        private System.Windows.Forms.Button cell3;
        private System.Windows.Forms.Button cell4;
        private System.Windows.Forms.Button cell5;
        private System.Windows.Forms.Button cell6;
        private System.Windows.Forms.Button cell7;
        private System.Windows.Forms.Button cell8;
        private System.Windows.Forms.Button cell9;
        private System.Windows.Forms.Label lblMove;
        private System.Windows.Forms.Timer tmrWinChk;
        private System.Windows.Forms.Timer tmrAI;
        private System.Windows.Forms.Button MM;
    }
}

