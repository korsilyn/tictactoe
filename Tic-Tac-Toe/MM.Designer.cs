﻿namespace Tic_Tac_Toe
{
    partial class MM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PvAI = new System.Windows.Forms.Button();
            this.Record = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.Settings = new System.Windows.Forms.Button();
            this.PvF = new System.Windows.Forms.Button();
            this.Version = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PvAI
            // 
            this.PvAI.BackColor = System.Drawing.SystemColors.ControlLight;
            this.PvAI.FlatAppearance.BorderSize = 0;
            this.PvAI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PvAI.Location = new System.Drawing.Point(12, 12);
            this.PvAI.Name = "PvAI";
            this.PvAI.Size = new System.Drawing.Size(244, 61);
            this.PvAI.TabIndex = 0;
            this.PvAI.Text = "Против ИИ";
            this.PvAI.UseVisualStyleBackColor = false;
            this.PvAI.Click += new System.EventHandler(this.PvAI_Click);
            // 
            // Record
            // 
            this.Record.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Record.FlatAppearance.BorderSize = 0;
            this.Record.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Record.Location = new System.Drawing.Point(12, 146);
            this.Record.Name = "Record";
            this.Record.Size = new System.Drawing.Size(244, 61);
            this.Record.TabIndex = 2;
            this.Record.Text = "Рекорды";
            this.Record.UseVisualStyleBackColor = false;
            this.Record.Click += new System.EventHandler(this.Record_Click);
            // 
            // Exit
            // 
            this.Exit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Exit.FlatAppearance.BorderSize = 0;
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Location = new System.Drawing.Point(12, 280);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(244, 61);
            this.Exit.TabIndex = 3;
            this.Exit.Text = "Выход";
            this.Exit.UseVisualStyleBackColor = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            this.Exit.MouseEnter += new System.EventHandler(this.Exit_MouseEnter);
            this.Exit.MouseLeave += new System.EventHandler(this.Exit_MouseLeave);
            // 
            // Settings
            // 
            this.Settings.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Settings.FlatAppearance.BorderSize = 0;
            this.Settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Settings.Location = new System.Drawing.Point(12, 213);
            this.Settings.Name = "Settings";
            this.Settings.Size = new System.Drawing.Size(244, 61);
            this.Settings.TabIndex = 4;
            this.Settings.Text = "Настройки";
            this.Settings.UseVisualStyleBackColor = false;
            this.Settings.Click += new System.EventHandler(this.Settings_Click);
            // 
            // PvF
            // 
            this.PvF.BackColor = System.Drawing.SystemColors.ControlLight;
            this.PvF.FlatAppearance.BorderSize = 0;
            this.PvF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PvF.Location = new System.Drawing.Point(12, 79);
            this.PvF.Name = "PvF";
            this.PvF.Size = new System.Drawing.Size(244, 61);
            this.PvF.TabIndex = 5;
            this.PvF.Text = "Против игрока (на одном ПК)";
            this.PvF.UseVisualStyleBackColor = false;
            this.PvF.Click += new System.EventHandler(this.PvF_Click);
            // 
            // Version
            // 
            this.Version.Location = new System.Drawing.Point(12, 348);
            this.Version.Name = "Version";
            this.Version.Size = new System.Drawing.Size(244, 21);
            this.Version.TabIndex = 6;
            this.Version.Text = "v3.4.0, by KrakeN Software, 2019";
            this.Version.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(268, 378);
            this.Controls.Add(this.Version);
            this.Controls.Add(this.PvF);
            this.Controls.Add(this.Settings);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.Record);
            this.Controls.Add(this.PvAI);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tic-Tac-Toe";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button PvAI;
        private System.Windows.Forms.Button Record;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button Settings;
        private System.Windows.Forms.Button PvF;
        private System.Windows.Forms.Label Version;
    }
}