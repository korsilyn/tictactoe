﻿using System;
using System.IO;

namespace Updater
{
    class Program
    {
        static void Main(string[] args)
        {
            File.Delete(Environment.CurrentDirectory + "/Tic-Tac-Toe.exe");
            File.Move(Environment.CurrentDirectory + "/Update/Tic-Tac-Toe.exe", Environment.CurrentDirectory + "/Tic-Tac-Toe.exe");
            System.Diagnostics.Process.Start(Environment.CurrentDirectory + "/Tic-Tac-Toe.exe");
        }
    }
}
