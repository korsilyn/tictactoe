﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tic_Tac_Toe
{
    public partial class Settings : Form
    {
        Methods met = new Methods();

        int theme, dif;
        string name, lang;

        public Settings()
        {
            InitializeComponent();
            met.LoadSet();
            PlayerName.Text = met.name;
            if (met.lang == "ru")
            {
                langBox.Text = "Русский";
                Theme.Text = "Тема";
                Day.Text = "Светлая";
                Night.Text = "Тёмная";
                AIDifficult.Text = "Уровень сложности ИИ";
                Easy.Text = "Легко";
                Middle.Text = "Средне";
                Hard.Text = "Сложно";
                NamePl.Text = "Имя";
                Language.Text = "Язык";
                Reset.Text = "Сбросить статистику";
                Save.Text = "Сохранить";
                Back.Text = "Отмена";
            }
            else if (met.lang == "en")
            {
                langBox.Text = "English";
                Theme.Text = "Theme";
                Day.Text = "Light";
                Night.Text = "Dark";
                AIDifficult.Text = "The level of complexity of AI";
                Easy.Text = "Easy";
                Middle.Text = "Middle";
                Hard.Text = "Hard";
                NamePl.Text = "Name";
                Language.Text = "Language";
                Reset.Text = "Reset statistics";
                Save.Text = "Save";
                Back.Text = "Cancel";
            }
            if (met.theme == 1)
            {
                Day.Checked = true;
                BackColor = DefaultBackColor;
                Theme.BackColor = Color.FromArgb(224, 224, 224);
                AIDifficult.BackColor = Color.FromArgb(224, 224, 224);
                NamePl.BackColor = Color.FromArgb(224, 224, 224);
                Reset.BackColor = Color.FromArgb(224, 224, 224);
                Save.BackColor = Color.FromArgb(224, 224, 224);
                Back.BackColor = Color.FromArgb(224, 224, 224);
                Language.BackColor = Color.FromArgb(224, 224, 224);
            }
            else
            {
                Night.Checked = true;
                BackColor = Color.FromArgb(40, 40, 40);
                Theme.BackColor = Color.FromArgb(100, 100, 100);
                AIDifficult.BackColor = Color.FromArgb(100, 100, 100);
                NamePl.BackColor = Color.FromArgb(100, 100, 100);
                Reset.BackColor = Color.FromArgb(100, 100, 100);
                Save.BackColor = Color.FromArgb(100, 100, 100);
                Back.BackColor = Color.FromArgb(100, 100, 100);
                Language.BackColor = Color.FromArgb(100, 100, 100);
            }
            if (met.dif == 1)
                Difficult.Value = 0;
            else if (met.dif == 2)
                Difficult.Value = 1;
            else
                Difficult.Value = 2;
        }

        private void Save_Click(object sender, EventArgs e)
        {
            SaveSet();
            Close();
            Application.Restart();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            Close();
            Program.main.Show();
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            File.WriteAllText("record.dat", string.Empty);
        }

        private void Settings_FormClosed(object sender, FormClosedEventArgs e)
        {
            Program.main.Show();
        }

        public void SaveSet()
        {
            if (Day.Checked == true)
                theme = 1;
            else
                theme = 2;
            if (Difficult.Value == 0)
                dif = 1;
            else if (Difficult.Value == 1)
                dif = 2;
            else
                dif = 3;
            if (PlayerName.Text == string.Empty)
                name = "Player";
            else
                name = PlayerName.Text;
            if (langBox.SelectedItem.ToString() == "Русский")
                lang = "ru";
            else if (langBox.SelectedItem.ToString() == "English")
                lang = "en";
            string temp;
            File.WriteAllText("settings.dat", string.Empty);
            using (StreamWriter save = File.AppendText("settings.dat"))
            {
                temp = theme.ToString();
                save.WriteLine(temp);
                temp = dif.ToString();
                save.WriteLine(temp);
                save.WriteLine(name);
                save.WriteLine(lang);
            }
        }
    }
}
