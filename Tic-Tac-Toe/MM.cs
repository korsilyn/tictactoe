﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AutoUpdaterDotNET;

namespace Tic_Tac_Toe
{
    public partial class MM : Form
    {
        Methods met = new Methods();

        public MM()
        {
            //Настройка AutoUpdater (Загрузка обновления в папку /Update)
            AutoUpdater.ApplicationExitEvent += AutoUpdater_ApplicationExitEvent;

            void AutoUpdater_ApplicationExitEvent()
            {
                Text = @"Closing application...";
                System.Diagnostics.Process.Start(Environment.CurrentDirectory + "/Updater.exe");
                Application.Exit();
            }
            AutoUpdater.DownloadPath = Environment.CurrentDirectory + "/Update";
            AutoUpdater.ShowRemindLaterButton = false;
            AutoUpdater.RunUpdateAsAdmin = false;
            AutoUpdater.Start("https://collor.ru/KrakeN/version.xml"); //PS не арбайт, т.к. в данный момент сервер отстутсвует

            InitializeComponent();
            met.LoadSet();

            //Выбор языка
            if (met.lang == "ru")
            {
                PvAI.Text = "Против ИИ";
                PvF.Text = "Против игрока (на одном ПК)";
                Record.Text = "Рекорды";
                Settings.Text = "Настройки";
                Exit.Text = "Выход";
            }
            else if (met.lang == "en")
            {
                PvAI.Text = "VS AI";
                PvF.Text = "VS other player (on one PC)";
                Record.Text = "Records";
                Settings.Text = "Settings";
                Exit.Text = "Exit";
            }

            //Выбор темы
            if (met.theme == 1)
            {
                BackColor = DefaultBackColor;
                PvAI.BackColor = Color.FromArgb(224, 224, 224);
                PvF.BackColor = Color.FromArgb(224, 224, 224);
                Record.BackColor = Color.FromArgb(224, 224, 224);
                Settings.BackColor = Color.FromArgb(224, 224, 224);
                Exit.BackColor = Color.FromArgb(224, 224, 224);
                Version.BackColor = Color.Transparent;
            }
            else
            {
                BackColor = Color.FromArgb(40,40,40);
                PvAI.BackColor = Color.FromArgb(100, 100, 100);
                PvF.BackColor = Color.FromArgb(100, 100, 100);
                Record.BackColor = Color.FromArgb(100, 100, 100);
                Settings.BackColor = Color.FromArgb(100, 100, 100);
                Exit.BackColor = Color.FromArgb(100, 100, 100);
                Version.BackColor = Color.FromArgb(100, 100, 100);
            }
        }

        //Выбор режима "Против ИИ"
        private void PvAI_Click(object sender, EventArgs e)
        {
            PvAI pvai = new PvAI();
            pvai.Show();
            Program.main.Hide();
        }

        //Выход
        private void Exit_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Спасибо за игру!", "Выход");
            Application.Exit();
        }

        //Покраска кнопки "Выход" при наведении
        private void Exit_MouseEnter(object sender, EventArgs e)
        {
            Exit.BackColor = Color.FromArgb(255,30,30);
        }
        private void Exit_MouseLeave(object sender, EventArgs e)
        {
            if (met.theme == 1)
                Exit.BackColor = Color.FromArgb(224, 224, 224);
            else
                Exit.BackColor = Color.FromArgb(100, 100, 100);
        }

        //Вход в Настройки
        private void Settings_Click(object sender, EventArgs e)
        {
            Settings set = new Settings();
            set.Show();
            Program.main.Hide();
        }

        //Выбор режима "Против друга"
        private void PvF_Click(object sender, EventArgs e)
        {
            PvF pvf = new PvF();
            pvf.Show();
            Program.main.Hide();
        }

        //Просмотр рекордов
        private void Record_Click(object sender, EventArgs e)
        {
            met.LoadScore();
            MessageBox.Show("Победы: " + met.win.ToString() + "\r\n" + "Поражения: " + met.lose.ToString() + "\r\n" + "Ничьи: " + met.draw.ToString() + "\r\n", "Рекорды");
        }
    }
}