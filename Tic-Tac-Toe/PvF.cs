﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tic_Tac_Toe
{
    public partial class PvF : Form
    {
        Methods met = new Methods();

        int move = 1;
        string player2, winner1, winner2, winner3, winner4; //So informative :D

        public PvF()
        {
            InitializeComponent();
            met.LoadSet();
            if (met.lang == "ru")
            {
                MM.Text = "В главное меню";
                player2 = "Игрок 2";
                winner1 = "Победитель: ";
                winner2 = "Победитель: " + player2;
                winner3 = "Исход";
                winner4 = "Ничья!";
            }
            else if (met.lang == "en")
            {
                MM.Text = "In main menu";
                player2 = "Player 2";
                winner1 = "Winner: ";
                winner2 = "Winner: " + player2;
                winner3 = "Result";
                winner4 = "Draw!";
            }
            lblMove.Text = met.name;
            if (met.theme == 1)
            {
                this.BackColor = Control.DefaultBackColor;
                cell1.BackColor = Color.White;
                cell2.BackColor = Color.White;
                cell3.BackColor = Color.White;
                cell4.BackColor = Color.White;
                cell5.BackColor = Color.White;
                cell6.BackColor = Color.White;
                cell7.BackColor = Color.White;
                cell8.BackColor = Color.White;
                cell9.BackColor = Color.White;
                MM.BackColor = Color.FromArgb(224, 224, 224);
                lblMove.BackColor = Color.Transparent;
            }
            else
            {
                this.BackColor = Color.FromArgb(40, 40, 40);
                cell1.BackColor = Color.FromArgb(100, 100, 100);
                cell2.BackColor = Color.FromArgb(100, 100, 100);
                cell3.BackColor = Color.FromArgb(100, 100, 100);
                cell4.BackColor = Color.FromArgb(100, 100, 100);
                cell5.BackColor = Color.FromArgb(100, 100, 100);
                cell6.BackColor = Color.FromArgb(100, 100, 100);
                cell7.BackColor = Color.FromArgb(100, 100, 100);
                cell8.BackColor = Color.FromArgb(100, 100, 100);
                cell9.BackColor = Color.FromArgb(100, 100, 100);
                MM.BackColor = Color.FromArgb(100, 100, 100);
                lblMove.BackColor = Color.FromArgb(100, 100, 100);
            }
        }
        
        int[] field = { 0, 0, 0, 0, 0, 0, 0, 0, 0 }; //Пустое - 0, Нолик - 2, Крестик - 1

        private void cell1_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[0] = 1;
                cell1.BackgroundImage = Properties.Resources.X;
                cell1.Enabled = false;
                move = 2;
                lblMove.Text = player2;
            }
            else
            {
                field[0] = 2;
                cell1.BackgroundImage = Properties.Resources.O;
                cell1.Enabled = false;
                move = 1;
                lblMove.Text = met.name;
            }
        }

        private void cell2_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[1] = 1;
                cell2.BackgroundImage = Properties.Resources.X;
                cell2.Enabled = false;
                move = 2;
                lblMove.Text = player2;
            }
            else
            {
                field[1] = 2;
                cell2.BackgroundImage = Properties.Resources.O;
                cell2.Enabled = false;
                move = 1;
                lblMove.Text = met.name;
            }
        }

        private void cell3_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[2] = 1;
                cell3.BackgroundImage = Properties.Resources.X;
                cell3.Enabled = false;
                move = 2;
                lblMove.Text = player2;
            }
            else
            {
                field[2] = 2;
                cell3.BackgroundImage = Properties.Resources.O;
                cell3.Enabled = false;
                move = 1;
                lblMove.Text = met.name;
            }
        }

        private void cell4_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[3] = 1;
                cell4.BackgroundImage = Properties.Resources.X;
                cell4.Enabled = false;
                move = 2;
                lblMove.Text = player2;
            }
            else
            {
                field[3] = 2;
                cell4.BackgroundImage = Properties.Resources.O;
                cell4.Enabled = false;
                move = 1;
                lblMove.Text = met.name;
            }
        }

        private void cell5_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[4] = 1;
                cell5.BackgroundImage = Properties.Resources.X;
                cell5.Enabled = false;
                move = 2;
                lblMove.Text = player2;
            }
            else
            {
                field[4] = 2;
                cell5.BackgroundImage = Properties.Resources.O;
                cell5.Enabled = false;
                move = 1;
                lblMove.Text = met.name;
            }
        }

        private void cell6_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[5] = 1;
                cell6.BackgroundImage = Properties.Resources.X;
                cell6.Enabled = false;
                move = 2;
                lblMove.Text = player2;
            }
            else
            {
                field[5] = 2;
                cell6.BackgroundImage = Properties.Resources.O;
                cell6.Enabled = false;
                move = 1;
                lblMove.Text = met.name;
            }
        }

        private void cell7_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[6] = 1;
                cell7.BackgroundImage = Properties.Resources.X;
                cell7.Enabled = false;
                move = 2;
                lblMove.Text = player2;
            }
            else
            {
                field[6] = 2;
                cell7.BackgroundImage = Properties.Resources.O;
                cell7.Enabled = false;
                move = 1;
                lblMove.Text = met.name;
            }
        }

        private void cell8_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[7] = 1;
                cell8.BackgroundImage = Properties.Resources.X;
                cell8.Enabled = false;
                move = 2;
                lblMove.Text = player2;
            }
            else
            {
                field[7] = 2;
                cell8.BackgroundImage = Properties.Resources.O;
                cell8.Enabled = false;
                move = 1;
                lblMove.Text = met.name;
            }
        }

        private void cell9_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[8] = 1;
                cell9.BackgroundImage = Properties.Resources.X;
                cell9.Enabled = false;
                move = 2;
                lblMove.Text = player2;
            }
            else
            {
                field[8] = 2;
                cell9.BackgroundImage = Properties.Resources.O;
                cell9.Enabled = false;
                move = 1;
                lblMove.Text = met.name;
            }
        }

        private void tmrWinChk_Tick(object sender, EventArgs e)
        {
            int winner;
            if (field[0] == field[1] && field[0] == field[2] && field[0] != 0)
            {
                tmrWinChk.Enabled = false;
                winner = field[0];
                if (winner == 1)
                    MessageBox.Show(winner1 + met.name, winner3);
                else
                    MessageBox.Show(winner2, winner3);
                Application.Restart();
            }
            else if (field[3] == field[4] && field[3] == field[5] && field[3] != 0)
            {
                tmrWinChk.Enabled = false;
                winner = field[3];
                if (winner == 1)
                    MessageBox.Show(winner1 + met.name, winner3);
                else
                    MessageBox.Show(winner2, winner3);
                Application.Restart();
            }
            else if (field[6] == field[7] && field[6] == field[8] && field[6] != 0)
            {
                tmrWinChk.Enabled = false;
                winner = field[6];
                if (winner == 1)
                    MessageBox.Show(winner1 + met.name, winner3);
                else
                    MessageBox.Show(winner2, winner3);
                Application.Restart();
            }
            else if (field[0] == field[3] && field[0] == field[6] && field[0] != 0)
            {
                tmrWinChk.Enabled = false;
                winner = field[0];
                if (winner == 1)
                    MessageBox.Show(winner1 + met.name, winner3);
                else
                    MessageBox.Show(winner2, winner3);
                Application.Restart();
            }
            else if (field[1] == field[4] && field[1] == field[7] && field[1] != 0)
            {
                tmrWinChk.Enabled = false;
                winner = field[1];
                if (winner == 1)
                    MessageBox.Show(winner1 + met.name, winner3);
                else
                    MessageBox.Show(winner2, winner3);
                Application.Restart();
            }
            else if (field[2] == field[5] && field[2] == field[8] && field[2] != 0)
            {
                tmrWinChk.Enabled = false;
                winner = field[2];
                if (winner == 1)
                    MessageBox.Show(winner1 + met.name, winner3);
                else
                    MessageBox.Show(winner2, winner3);
                Application.Restart();
            }
            else if (field[0] == field[4] && field[0] == field[8] && field[0] != 0)
            {
                tmrWinChk.Enabled = false;
                winner = field[0];
                if (winner == 1)
                    MessageBox.Show(winner1 + met.name, winner3);
                else
                    MessageBox.Show(winner2, winner3);
                Application.Restart();
            }
            else if (field[2] == field[4] && field[2] == field[6] && field[2] != 0)
            {
                tmrWinChk.Enabled = false;
                winner = field[2];
                if (winner == 1)
                    MessageBox.Show(winner1 + met.name, winner3);
                else
                    MessageBox.Show(winner2, winner3);
                Application.Restart();
            }
            else if (field[0] != 0 && field[1] != 0 && field[2] != 0 && field[3] != 0 && field[4] != 0 && field[5] != 0 && field[6] != 0 && field[7] != 0 && field[8] != 0)
            {
                tmrWinChk.Enabled = false;
                MessageBox.Show(winner4, winner3);
                Application.Restart();
            }
        }

        private void MM_Click(object sender, EventArgs e)
        {
            Close();
            Program.main.Show();
        }

        private void PvF_FormClosed(object sender, FormClosedEventArgs e)
        {
            Program.main.Show();
        }
    }
}
