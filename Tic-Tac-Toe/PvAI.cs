﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tic_Tac_Toe
{
    public partial class PvAI : Form 
    {
        Methods met = new Methods();

        Random rnd = new Random();
        int move;
        string movetxt1, movetxt2, winner1, winner2, winner3, winner4;

        public PvAI()
        {
            InitializeComponent();
            met.LoadSet();
            if (met.lang == "ru")
            {
                MM.Text = "В главное меню";
                movetxt1 = "Ваш ход";
                movetxt2 = "Ход противника";
                winner1 = "Победитель: ";
                winner2 = "Победитель: ИИ";
                winner3 = "Исход";
                winner4 = "Ничья!";
            }
            else if (met.lang == "en")
            {
                MM.Text = "In main menu";
                movetxt1 = "Your turn";
                movetxt2 = "Opponent turn";
                winner1 = "Winner: ";
                winner2 = "Winner: AI";
                winner3 = "Result";
                winner4 = "Draw!";
            }
            if (met.theme == 1)
            {
                BackColor = DefaultBackColor;
                cell1.BackColor = Color.White;
                cell2.BackColor = Color.White;
                cell3.BackColor = Color.White;
                cell4.BackColor = Color.White;
                cell5.BackColor = Color.White;
                cell6.BackColor = Color.White;
                cell7.BackColor = Color.White;
                cell8.BackColor = Color.White;
                cell9.BackColor = Color.White;
                MM.BackColor = Color.FromArgb(224, 224, 224);
                lblMove.BackColor = Color.Transparent; 
            }
            else
            {
                BackColor = Color.FromArgb(40, 40, 40);
                cell1.BackColor = Color.FromArgb(100, 100, 100);
                cell2.BackColor = Color.FromArgb(100, 100, 100);
                cell3.BackColor = Color.FromArgb(100, 100, 100);
                cell4.BackColor = Color.FromArgb(100, 100, 100);
                cell5.BackColor = Color.FromArgb(100, 100, 100);
                cell6.BackColor = Color.FromArgb(100, 100, 100);
                cell7.BackColor = Color.FromArgb(100, 100, 100);
                cell8.BackColor = Color.FromArgb(100, 100, 100);
                cell9.BackColor = Color.FromArgb(100, 100, 100);
                MM.BackColor = Color.FromArgb(100, 100, 100);
                lblMove.BackColor = Color.FromArgb(100, 100, 100);
            }
            move = rnd.Next(1, 3);
            if (move == 2)
            {
                tmrAI.Start();
                lblMove.Text = movetxt2;
            }
            else
                lblMove.Text = movetxt1;
            met.LoadScore();
        }

        //ИИ "обдумывает" ход 0,5 секунд
        int[] field = { 0, 0, 0, 0, 0, 0, 0, 0, 0 }; //Пустое - 0, Нолик - 2, Крестик - 1

        void AI_Easy()
        {
            if (move == 2)
            {
                //Ход
                if (field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 0)
                {
                    field[1] = 2;
                    cell2.BackgroundImage = Properties.Resources.O;
                    cell2.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[5] == 0)
                {
                    field[5] = 2;
                    cell6.BackgroundImage = Properties.Resources.O;
                    cell6.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[7] == 0)
                {
                    field[7] = 2;
                    cell8.BackgroundImage = Properties.Resources.O;
                    cell8.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 0)
                {
                    field[3] = 2;
                    cell4.BackgroundImage = Properties.Resources.O;
                    cell4.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
            }
        }
        void AI_Mid()
        {
            if (move == 2)
            {
                //Защита
                if (field[0] == 1 && field[1] == 1 && field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 1 && field[2] == 1 && field[1] == 0)
                {
                    field[1] = 2;
                    cell2.BackgroundImage = Properties.Resources.O;
                    cell2.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 1 && field[2] == 1 && field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 1 && field[4] == 1 && field[5] == 0)
                {
                    field[5] = 2;
                    cell6.BackgroundImage = Properties.Resources.O;
                    cell6.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 1 && field[5] == 1 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 1 && field[5] == 1 && field[3] == 0)
                {
                    field[3] = 2;
                    cell4.BackgroundImage = Properties.Resources.O;
                    cell4.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[6] == 1 && field[7] == 1 && field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[6] == 1 && field[8] == 1 && field[7] == 0)
                {
                    field[7] = 2;
                    cell8.BackgroundImage = Properties.Resources.O;
                    cell8.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[7] == 1 && field[8] == 1 && field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 1 && field[3] == 1 && field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 1 && field[6] == 1 && field[3] == 0)
                {
                    field[3] = 2;
                    cell4.BackgroundImage = Properties.Resources.O;
                    cell4.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 1 && field[6] == 1 && field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 1 && field[4] == 1 && field[7] == 0)
                {
                    field[7] = 2;
                    cell8.BackgroundImage = Properties.Resources.O;
                    cell8.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 1 && field[7] == 1 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 1 && field[7] == 1 && field[1] == 0)
                {
                    field[1] = 2;
                    cell2.BackgroundImage = Properties.Resources.O;
                    cell2.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 1 && field[5] == 1 && field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 1 && field[8] == 1 && field[5] == 0)
                {
                    field[5] = 2;
                    cell6.BackgroundImage = Properties.Resources.O;
                    cell6.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[5] == 1 && field[8] == 1 && field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 1 && field[4] == 1 && field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 1 && field[8] == 1 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 1 && field[8] == 1 && field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 1 && field[4] == 1 && field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 1 && field[6] == 1 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 1 && field[6] == 1 && field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 0)
                {
                    field[1] = 2;
                    cell2.BackgroundImage = Properties.Resources.O;
                    cell2.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[5] == 0)
                {
                    field[5] = 2;
                    cell6.BackgroundImage = Properties.Resources.O;
                    cell6.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[7] == 0)
                {
                    field[7] = 2;
                    cell8.BackgroundImage = Properties.Resources.O;
                    cell8.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 0)
                {
                    field[3] = 2;
                    cell4.BackgroundImage = Properties.Resources.O;
                    cell4.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
            }
        }
        void AI_Hard ()
        {
            if (move == 2)
            {
                //Атака
                if (field[0] == 2 && field[1] == 2 && field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 2 && field[2] == 2 && field[1] == 0)
                {
                    field[1] = 2;
                    cell2.BackgroundImage = Properties.Resources.O;
                    cell2.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 2 && field[2] == 2 && field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 2 && field[4] == 2 && field[5] == 0)
                {
                    field[5] = 2;
                    cell6.BackgroundImage = Properties.Resources.O;
                    cell6.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 2 && field[5] == 2 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 2 && field[5] == 2 && field[3] == 0)
                {
                    field[3] = 2;
                    cell4.BackgroundImage = Properties.Resources.O;
                    cell4.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[6] == 2 && field[7] == 2 && field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[6] == 2 && field[8] == 2 && field[7] == 0)
                {
                    field[7] = 2;
                    cell8.BackgroundImage = Properties.Resources.O;
                    cell8.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[7] == 2 && field[8] == 2 && field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 2 && field[3] == 2 && field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 2 && field[6] == 2 && field[3] == 0)
                {
                    field[3] = 2;
                    cell4.BackgroundImage = Properties.Resources.O;
                    cell4.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 2 && field[6] == 2 && field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 2 && field[4] == 2 && field[7] == 0)
                {
                    field[7] = 2;
                    cell8.BackgroundImage = Properties.Resources.O;
                    cell8.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 2 && field[7] == 2 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 2 && field[7] == 2 && field[1] == 0)
                {
                    field[1] = 2;
                    cell2.BackgroundImage = Properties.Resources.O;
                    cell2.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 2 && field[5] == 2 && field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 2 && field[8] == 2 && field[5] == 0)
                {
                    field[5] = 2;
                    cell6.BackgroundImage = Properties.Resources.O;
                    cell6.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[5] == 2 && field[8] == 2 && field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 2 && field[4] == 2 && field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 2 && field[8] == 2 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 2 && field[8] == 2 && field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 2 && field[4] == 2 && field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 2 && field[6] == 2 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 2 && field[6] == 2 && field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                //Защита
                else if (field[0] == 1 && field[1] == 1 && field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 1 && field[2] == 1 && field[1] == 0)
                {
                    field[1] = 2;
                    cell2.BackgroundImage = Properties.Resources.O;
                    cell2.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 1 && field[2] == 1 && field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 1 && field[4] == 1 && field[5] == 0)
                {
                    field[5] = 2;
                    cell6.BackgroundImage = Properties.Resources.O;
                    cell6.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 1 && field[5] == 1 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 1 && field[5] == 1 && field[3] == 0)
                {
                    field[3] = 2;
                    cell4.BackgroundImage = Properties.Resources.O;
                    cell4.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[6] == 1 && field[7] == 1 && field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[6] == 1 && field[8] == 1 && field[7] == 0)
                {
                    field[7] = 2;
                    cell8.BackgroundImage = Properties.Resources.O;
                    cell8.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[7] == 1 && field[8] == 1 && field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 1 && field[3] == 1 && field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 1 && field[6] == 1 && field[3] == 0)
                {
                    field[3] = 2;
                    cell4.BackgroundImage = Properties.Resources.O;
                    cell4.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 1 && field[6] == 1 && field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 1 && field[4] == 1 && field[7] == 0)
                {
                    field[7] = 2;
                    cell8.BackgroundImage = Properties.Resources.O;
                    cell8.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 1 && field[7] == 1 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 1 && field[7] == 1 && field[1] == 0)
                {
                    field[1] = 2;
                    cell2.BackgroundImage = Properties.Resources.O;
                    cell2.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 1 && field[5] == 1 && field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 1 && field[8] == 1 && field[5] == 0)
                {
                    field[5] = 2;
                    cell6.BackgroundImage = Properties.Resources.O;
                    cell6.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[5] == 1 && field[8] == 1 && field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 1 && field[4] == 1 && field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[0] == 1 && field[8] == 1 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 1 && field[8] == 1 && field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 1 && field[4] == 1 && field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 1 && field[6] == 1 && field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[4] == 1 && field[6] == 1 && field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                //Ход
                else if (field[4] == 0)
                {
                    field[4] = 2;
                    cell5.BackgroundImage = Properties.Resources.O;
                    cell5.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if(field[0] == 0)
                {
                    field[0] = 2;
                    cell1.BackgroundImage = Properties.Resources.O;
                    cell1.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[1] == 0)
                {
                    field[1] = 2;
                    cell2.BackgroundImage = Properties.Resources.O;
                    cell2.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[2] == 0)
                {
                    field[2] = 2;
                    cell3.BackgroundImage = Properties.Resources.O;
                    cell3.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[5] == 0)
                {
                    field[5] = 2;
                    cell6.BackgroundImage = Properties.Resources.O;
                    cell6.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[8] == 0)
                {
                    field[8] = 2;
                    cell9.BackgroundImage = Properties.Resources.O;
                    cell9.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[7] == 0)
                {
                    field[7] = 2;
                    cell8.BackgroundImage = Properties.Resources.O;
                    cell8.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[6] == 0)
                {
                    field[6] = 2;
                    cell7.BackgroundImage = Properties.Resources.O;
                    cell7.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
                else if (field[3] == 0)
                {
                    field[3] = 2;
                    cell4.BackgroundImage = Properties.Resources.O;
                    cell4.Enabled = false;
                    tmrAI.Enabled = false;
                    move = 1;
                    lblMove.Text = movetxt1;
                }
            }
        }

        private void cell1_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[0] = 1;
                cell1.BackgroundImage = Properties.Resources.X;
                cell1.Enabled = false;
                tmrAI.Enabled = true;
                move = 2;
                lblMove.Text = movetxt2;
            }
        }

        private void cell2_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[1] = 1;
                cell2.BackgroundImage = Properties.Resources.X;
                cell2.Enabled = false;
                tmrAI.Enabled = true;
                move = 2;
                lblMove.Text = movetxt2;
            }
        }

        private void cell3_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[2] = 1;
                cell3.BackgroundImage = Properties.Resources.X;
                cell3.Enabled = false;
                tmrAI.Enabled = true;
                move = 2;
                lblMove.Text = movetxt2;
            }
        }

        private void cell4_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[3] = 1;
                cell4.BackgroundImage = Properties.Resources.X;
                cell4.Enabled = false;
                tmrAI.Enabled = true;
                move = 2;
                lblMove.Text = movetxt2;
            }
        }

        private void cell5_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[4] = 1;
                cell5.BackgroundImage = Properties.Resources.X;
                cell5.Enabled = false;
                tmrAI.Enabled = true;
                move = 2;
                lblMove.Text = movetxt2;
            }
        }

        private void cell6_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[5] = 1;
                cell6.BackgroundImage = Properties.Resources.X;
                cell6.Enabled = false;
                tmrAI.Enabled = true;
                move = 2;
                lblMove.Text = movetxt2;
            }
        }

        private void cell7_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[6] = 1;
                cell7.BackgroundImage = Properties.Resources.X;
                cell7.Enabled = false;
                tmrAI.Enabled = true;
                move = 2;
                lblMove.Text = movetxt2;
            }
        }

        private void cell8_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[7] = 1;
                cell8.BackgroundImage = Properties.Resources.X;
                cell8.Enabled = false;
                tmrAI.Enabled = true;
                move = 2;
                lblMove.Text = movetxt2;
            }
        }

        private void cell9_Click(object sender, EventArgs e)
        {
            if (move == 1)
            {
                field[8] = 1;
                cell9.BackgroundImage = Properties.Resources.X;
                cell9.Enabled = false;
                tmrAI.Enabled = true;
                move = 2;
                lblMove.Text = movetxt2;
            }
        }

        private void tmrWinChk_Tick(object sender, EventArgs e)
        {
            int winner;
            if (field[0] == field[1] && field[0] == field[2] && field[0] != 0)
            {
                tmrWinChk.Enabled = false;
                tmrAI.Enabled = false;
                winner = field[0];
                if (winner == 1)
                {
                    met.win++;
                    MessageBox.Show(winner1 + met.name, winner3);
                }
                else
                {
                    met.lose++;
                    MessageBox.Show(winner2, winner3);
                }
                met.SaveScore();
                Application.Restart();
            }
            else if(field[3] == field[4] && field[3] == field[5] && field[3] != 0)
            {
                tmrWinChk.Enabled = false;
                tmrAI.Enabled = false;
                winner = field[3];
                if (winner == 1)
                {
                    met.win++;
                    MessageBox.Show(winner1 + met.name, winner3);
                }
                else
                {
                    met.lose++;
                    MessageBox.Show(winner2, winner3);
                }
                met.SaveScore();
                Application.Restart();
            }
            else if (field[6] == field[7] && field[6] == field[8] && field[6] != 0)
            {
                tmrWinChk.Enabled = false;
                tmrAI.Enabled = false;
                winner = field[6];
                if (winner == 1)
                {
                    met.win++;
                    MessageBox.Show(winner1 + met.name, winner3);
                }
                else
                {
                    met.lose++;
                    MessageBox.Show(winner2, winner3);
                }
                met.SaveScore();
                Application.Restart();
            }
            else if (field[0] == field[3] && field[0] == field[6] && field[0] != 0)
            {
                tmrWinChk.Enabled = false;
                tmrAI.Enabled = false;
                winner = field[0];
                if (winner == 1)
                {
                    met.win++;
                    MessageBox.Show(winner1 + met.name, winner3);
                }
                else
                {
                    met.lose++;
                    MessageBox.Show(winner2, winner3);
                }
                met.SaveScore();
                Application.Restart();
            }
            else if (field[1] == field[4] && field[1] == field[7] && field[1] != 0)
            {
                tmrWinChk.Enabled = false;
                tmrAI.Enabled = false;
                winner = field[1];
                if (winner == 1)
                {
                    met.win++;
                    MessageBox.Show(winner1 + met.name, winner3);
                }
                else
                {
                    met.lose++;
                    MessageBox.Show(winner2, winner3);
                }
                met.SaveScore();
                Application.Restart();
            }
            else if (field[2] == field[5] && field[2] == field[8] && field[2] != 0)
            {
                tmrWinChk.Enabled = false;
                tmrAI.Enabled = false;
                winner = field[2];
                if (winner == 1)
                {
                    met.win++;
                    MessageBox.Show(winner1 + met.name, winner3);
                }
                else
                {
                    met.lose++;
                    MessageBox.Show(winner2, winner3);
                }
                met.SaveScore();
                Application.Restart();
            }
            else if (field[0] == field[4] && field[0] == field[8] && field[0] != 0)
            {
                tmrWinChk.Enabled = false;
                tmrAI.Enabled = false;
                winner = field[0];
                if (winner == 1)
                {
                    met.win++;
                    MessageBox.Show(winner1 + met.name, winner3);
                }
                else
                {
                    met.lose++;
                    MessageBox.Show(winner2, winner3);
                }
                met.SaveScore();
                Application.Restart();
            }
            else if (field[2] == field[4] && field[2] == field[6] && field[2] != 0)
            {
                tmrWinChk.Enabled = false;
                tmrAI.Enabled = false;
                winner = field[2];
                if (winner == 1)
                {
                    met.win++;
                    MessageBox.Show(winner1 + met.name, winner3);
                }
                else
                {
                    met.lose++;
                    MessageBox.Show(winner2, winner3);
                }
                met.SaveScore();
                Application.Restart();
            }
            else if (field[0] != 0 && field[1] != 0 && field[2] != 0 && field[3] != 0 && field[4] != 0 && field[5] != 0 && field[6] != 0 && field[7] != 0 && field[8] != 0)
            {
                tmrWinChk.Enabled = false;
                tmrAI.Enabled = false;
                met.draw++;
                met.SaveScore();
                MessageBox.Show(winner4, winner3);
                Application.Restart();
            }
        }

        private void tmrAI_Tick(object sender, EventArgs e)
        {
            if (met.dif == 1)
                AI_Easy();
            else if (met.dif == 2)
                AI_Mid();
            else if (met.dif == 3)
                AI_Hard();
        }

        private void MM_Click(object sender, EventArgs e)
        {
            met.lose++;
            met.SaveScore();
            Close();
            Program.main.Show();
        }

        private void PvAI_FormClosed(object sender, FormClosedEventArgs e)
        {
            Program.main.Show();
        }
    }
}
