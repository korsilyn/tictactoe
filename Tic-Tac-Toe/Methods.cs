﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tic_Tac_Toe
{
    class Methods
    {
        public int win, lose, draw, theme, dif;
        public string name, lang;

        public void LoadSet()
        {
            string[] load;
            //Дефолтные настройки
            using (StreamWriter save = File.AppendText("settings.dat"))
            {
                save.WriteLine("1");
                save.WriteLine("2");
                save.WriteLine("Player");
                save.WriteLine("ru");
            }
            load = File.ReadAllLines("settings.dat");
            if (load[0] != string.Empty) //Конвертация в числа
            {
                theme = int.Parse(load[0]);
                dif = int.Parse(load[1]);
                name = load[2];
                lang = load[3];
            }
        }

        public void LoadScore()
        {
            string[] load = new string[3];
            using (StreamWriter save = File.AppendText("record.dat"))
            {
                save.WriteLine(string.Empty);
            }
            load = File.ReadAllLines("record.dat");
            if (load[0] != string.Empty)
            {
                win = int.Parse(load[0]);
                lose = int.Parse(load[1]);
                draw = int.Parse(load[2]);
            }
        }

        public void SaveScore()
        {
            string temp;
            File.WriteAllText("record.dat", string.Empty);
            using (StreamWriter save = File.AppendText("record.dat"))
            {
                temp = win.ToString();
                save.WriteLine(temp);
                temp = lose.ToString();
                save.WriteLine(temp);
                temp = draw.ToString();
                save.WriteLine(temp);
            }
        }
    }
}
